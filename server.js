var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.listen(port);
console.log("API molona esuchando en el puerto " + port);

app.get("/apitechu/v1",
 function(req, res){
  console.log("GET /apitechu/v1");
  res.send({"msg" : "Hola desde API TechU"});
 }
);
app.get("/apitechu/v1/users",
  function(req, res){
    console.log(" GET /apitechu/v1/users");
    res.sendFile('usuarios.json',{root:__dirname})
    //var users = require('./usuarios.json');
    //res.send(users);
  }
);

//POST creación de usuarios
app.post("/apitechu/v1/users",
    function(req, res){
      console.log(" POST /apitechu/v1/users");
      //console.log(req.headers);
      console.log("first_name" + req.body.first_name);
      console.log("last_name" + req.body.last_name);
      console.log("country"+ req.body.country);
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country    };

    var users = require('./usuarios.json');
    // push es un método de los arrays
    users.push(newUser);
    //console.log("Usuario añadido con éxito");

    writeUserDataToFile(users);
    console.log("Usuario guardado con éxito")
    res.send({"msg" : "Usuario guardado con éxito"});
        //res.send(users);
      }
)

app.delete("/apitechu/v1/users/:id",
    function(req, res){
      console.log(" DELETE /apitechu/v1/users/:id");
      console.log(req.params);
      console.log(req.params.id);

      var users = require('./usuarios.json');
      users.splice(req.params.id -1, 1);
      writeUserDataToFile(users);
      console.log("Usuario borrado");
      res.send({"msg" : "Usuario borrado"});
  }
);
function writeUserDataToFile(data){

  var fs =require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json",jsonUserData, "utf8",
    function(err){
      if (err){
        console.log(err);
      } else {
        console.log("Datos escritos en el archivo")
      }
    }
  )
}
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("headers");
    console.log(req.headers);
  }

)
app.post("/apitechu/v1/login",
function(req,res){
  console.log(" POST /apitechu/v1/login");
  var users = require('./usuarios.json');

  var email = req.body.email;
  var password = req.body.password;
  console.log("email : " + email);
  console.log("password : " + password);
  var msg = "Login incorrecto";
  for (user of users){

    console.log("user.email dentro del for "+ user.email)
    if ((email==user.email)&&(password==user.password)) {
      var userId = user.id;
      user.logged = true;
      msg = "Login correcto";
    }
  }
  writeUserDataToFile(users);
  res.send({"msg" : msg,"id" : userId});
})




app.post("/apitechu/v1/logout",
function(req,res){
  console.log(" POST /apitechu/v1/logout");
  var users = require('./usuarios.json');
  var id = req.body.id;
  console.log("id : " + id);
  msg="";
  for (user of users){
    if (id == user.id){
      if (user.logged==true) {
        msg = "Logout correcto";
        delete user.logged;
      } else {
        msg = "Logout incorrecto";
      }
    }
  }
writeUserDataToFile(users);
res.send(msg);
})
